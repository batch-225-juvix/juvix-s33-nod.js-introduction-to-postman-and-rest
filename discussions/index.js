//  Nod.js - Introduction to Postman and REST
// hidden complexity -> tago mo na ung di kelangan ipakita.
// so code po si API no sir, di po siya software or website.
// parang kukunin ng ibang application yung data sa app na naka register ka ganun sir? Ans: YES
// REST or restful api

// maganda sir na analogy is robot si backend... tapos ung API is ung language na ginawa nung inventor kung pano siya kakausapin ung robot para magamit ng iba... kelangan ung frontend/client/user marunong nung language na un para ma control/makausap si robot... pero si inventor di niya nilalabas ung buong language, meron syang commands na sya lang nakakaalam di na nya pinaalam sa iba. Ans: yes tama yung example mo.

// Synchronous means diridiritso.
// showstopper or blocking po ung error di na tumuloy kapag merong error.
// ===================================

// [SECTION] Javascript Synchronous vs Asynchronous
// Javascript is by default is synchronous meaning that only one statement is executed at a time.
console.log("hello world");



console.log("GoodBye")

// Asynchronous meaning that we can proceed to execute another statements, while time consuming code is running in the background.
// Asynchronous meaning maghihintay tayo ng input bago break ating results.
// mas inuuna niya yung madali yan ata yung example na nang asynchronous


// The Fetch API

/*
	- Allows you to asynchronously request for a resource (data)
	- A "promise" is an object that represents the eventual completion ( or will become a failure) of an asynchronous function and it's resulting value.
	- kapag naka,promise, nag,wwait pa siya ng response. (my response body na sya kasama)

	it returns
	- result or data.
	- Pending.
	- Error.

	SYNTAX:
		fetch('URL')
*/

// Promise Pending
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// SYNTAX
/*

	fetch('URL')
	.then((response) => statement)

*/

// Retrieving all posts following the REST API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise

// The 'fetch' method will return a "promise" that resolve to a "Response" Object.
// The 'then' method captures the "response" object and returns another 'promise' which will eventually be "resolve" "rejected";

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => console.log(response.status));

// ============= With JSON RESPONSE ============
// NOTE: kapag gumagamit ng .then dapat hindi ka naka ; para hindi mag end kasi dudugtungan pa natin ito.
// Q: bale sir yung nakapagpaconvert sa json pabalik ng js ay yung response.json sir no then icconsole? Ans: YES
// convertion .then((response) => response.json()) tapos hiwalay yung fetching .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts')
// Use the 'json' method from the 'response' object to convert data retrieve into JSON format to be used in our application.

.then((response) => response.json())

// Print the converted JSON value from the "fetch" request.
// Using multiple "then" methods creates a "promise chain"

.then((json) => console.log(json));



// [SECTION] Getting a specific post allowing the REST API (retrieve, /posts/:id, GET)
// id means GET
// mas inuuna niya yung madali yan ata yung example na nang asynchronous.
// parang find lang ito sa mongodb.
// parang nag ququery lng tayo.
// sa network kahelera ng console mas accurate yung pagkakasunod ng data pagdating sa fetching.
// pag asynchronous... kung ano mauna un ung sequence rin sa console. Kung ano una matapos.
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => console.log(json));


// ========================================
// [SECTION] Creating a post

/*

	fetch('URL', options)
	.then((response)=> {})
	.then((response)=> {})

*/

// Create a new post following the REST API (create, /posts, POST)
fetch('https://jsonplaceholder.typicode.com/posts', {

	// Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
	
	

	method: 'POST',
	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure

	headers: {
		'Content-type': 'application/json',
	},	
	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World',
		userId: 101
	})
}).then((response) => response.json())
.then((json) => console.log(json));


// ===============================================
// [Section] Updating a post using PUT method

// Updates a specific post following the Rest API (update, /posts/:id, PUT)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	id: 1,
	  	title: 'Updated post',
	  	body: 'Hello again!',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a post using PATCH method

// Updates a specific post following the Rest API (update, /posts/:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed
// PATCH is used to update the whole object
// PUT is used to update a single/several properties
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Corrected post',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Deleting a post

// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE'
});

// put means general?? patch means specific